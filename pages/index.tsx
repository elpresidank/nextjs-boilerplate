import classes from '../styles/Home.module.scss'
import Header from "../components/layout/Header";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Slider from "../components/content/Carousel/Slider";
import Carousel from "../components/content/Carousel/Carousel";
import {
  faChevronLeft
} from "@fortawesome/pro-solid-svg-icons";

function Home() {
  return (
    <div>
      <div className={classes.ribbonBanner}>
        <FontAwesomeIcon icon={faChevronLeft}/>
        <p>Powered by DankerEats</p>
      </div>
      <Header />
      <Carousel />
      <main>

      </main>
      <footer>

      </footer>
    </div>
  )
}

export default Home;
