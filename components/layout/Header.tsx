import classes from './Header.module.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
  faMapMarkerAlt,
  faPhone
} from "@fortawesome/pro-duotone-svg-icons";
import Link from "next/link";

function Header() {
  return (
    <div className={classes.header}>
      <div className={classes.header__left}>
        <h2>Tea Steak House &</h2>
        <h2>O'Toole's Bar</h2>
      </div>
      <div className={classes.header__right}>
        <div className={classes.header__contact}>
          <ol>
            <li className="address">
              <FontAwesomeIcon icon={faMapMarkerAlt}/>
              <p>215 S Main St, Tea, SD 57064</p>
            </li>
            <li className="phone">
              <FontAwesomeIcon icon={faPhone}/>
              <p>6053689667</p>
            </li>
            <li className={classes.link}>
              <a href="http://twitter.com/gordonramsayGRR" target="_blank" className={classes.link} title="Twitter">
                <FontAwesomeIcon icon={['fab', 'twitter']}/>
              </a>
              <a href="http://www.facebook.com/BreadStreetKitchen" target="_blank" className={classes.link}
                 title="Facebook">
                <FontAwesomeIcon icon={['fab', 'facebook']}/>
              </a>
              <a href="https://www.instagram.com/breadstreetkitchen/" target="_blank" className={classes.link}
                 title="Instagram">
                <FontAwesomeIcon icon={['fab', 'instagram']}/>
              </a>
            </li>
          </ol>
        </div>
        <nav className={classes.header__nav}>
          <ul>
            <li>
              <Link href='/'>Book a Table</Link>
            </li>
            <li>
              <Link href='/'>Menus</Link>
            </li>
            <li>
              <Link href='/'>Gallery</Link>
            </li>
            <li>
              <Link href='/'>Contact</Link>
            </li>
            <li>
              <Link href='/'>Gifts</Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Header;