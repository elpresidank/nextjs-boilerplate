import {useRef, useEffect, useState} from "react";
import classes from "./Carousel.module.scss";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronLeft, faChevronRight} from "@fortawesome/pro-duotone-svg-icons";

function Carousel() {
  const [sectionIndex, setSectionIndex] = useState(0);
  let carouselArr = [0, 1, 2, 3];

  const positionHandler = (index: number) => {
    setSectionIndex(index);
  }

  const goRight = () => {

    if (!(sectionIndex >= (carouselArr.length - 1))) {
      setSectionIndex(sectionIndex + 1);
    }
  }

  const goLeft = () => {
    if (!(sectionIndex === 0)) {
      setSectionIndex(sectionIndex - 1);
    }
  }

  return (
    <div className={classes.container}>
      <div className={classes.carousel}>
        <div className={classes.slider} style={{
          transform: `translate(${sectionIndex * -25}%)`
        }}>
          {carouselArr.map((item, index) => {
            return (
              <section key={index}>{`content for section ${index + 1}`}</section>
            )
          })}
        </div>
        <div className={classes.controls}>
          <button className={classes.goLeft} onClick={goLeft}>
            <FontAwesomeIcon icon={faChevronLeft}/>
          </button>
          <button className={classes.goRight} onClick={goRight}>
            <FontAwesomeIcon icon={faChevronRight}/>
          </button>
          <ul>
            {carouselArr.map((item, index) => {
              if (sectionIndex === index) {
                return (
                  <li key={index} className={classes.selected}/>
                )
              } else {
                return (<li key={index} onClick={() => positionHandler(index)}/>)
              }
            })}
          </ul>
        </div>

      </div>

    </div>
  )
}

export default Carousel;