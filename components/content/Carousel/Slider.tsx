// import classes from './Slider.module.scss'
// import {useState} from "react";
// import SliderItem from "./SliderItem";
// import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
// import { faChevronLeft, faChevronRight} from "@fortawesome/pro-duotone-svg-icons";
// import data from "../../../assets/data/images.json";
//
//
// function Slider() {
//
//   let sliderArr = [<SliderItem src='/img/gallery/dining-area.jpg' />, <SliderItem src='/img/gallery/cocktail.jpg' />] // just numbers for now
//   const [x, setX] = useState(0);
//   const goLeft = () => {
//     x === 0 ? setX(-100 * (sliderArr.length - 1)) : setX(x + 100);
//   }
//
//   const goRight = () => {
//     console.log(x)
//     x === -100 * (sliderArr.length - 1)?setX(0) : setX(x - 100);
//   }
//
//   return (
//     <div className={classes.slider}>
//       {
//         sliderArr.map((item, index) => {
//           return (
//             <div key={index} className={classes.slide} style={{transform: `translateX(${x}%)`, position: 'relative'}}>
//               {item}
//             </div>
//
//           )
//         })}
//       <button className={classes.goLeft} onClick={goLeft}>
//         <FontAwesomeIcon icon={faChevronLeft} />
//       </button>
//       <button className={classes.goRight} onClick={goRight}>
//         <FontAwesomeIcon icon={faChevronRight} />
//       </button>
//     </div>
//
//   )
// }
//
// export default Slider;